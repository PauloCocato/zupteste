//
//  ZupTestSearch.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import XCTest

@testable import ZupTeste

class ZupTestSearch : XCTestCase {
    var connector: CCOMDbConnector?
    
    override func setUp() {
        connector = CCOMDbConnector()
    }
    
    func testSearchMovieByName() {
        let expectation = self.expectation(description: "Test Async search for movie")
        
        
        
        connector!.searchMovieByName("art", type: nil, year: nil) { (success, result, error) -> Void in
            XCTAssertEqual(true, success, "failed searching for movie")
            expectation.fulfill()
        }
        
        
        waitForExpectations(timeout: 15, handler: nil)
        
        
    }
    
    
    func testGetMovieById() {
        let expectation = self.expectation(description: "Test Async search for movie")
        
        
        
        connector!.getMovieById("tt1285016", completionHandler: { (success, result, error) -> Void in
            
            XCTAssertEqual(true, success, "failed searching for movie")
            expectation.fulfill()
            
        })
        
        
        waitForExpectations(timeout: 15, handler: nil)
        
        
    }
    
}
