//
//  ZupSearchConfigTest.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import XCTest

@testable import ZupTeste

class ZupSearchConfigTest: XCTestCase {
    
    func testConfigUtil() {
        let apibaseUrl = ConfigUtil.getConfigValueForKey("apiBaseUrl")
        
        
        XCTAssertNotNil(apibaseUrl, "expected apiBaseUrl not to be nil, but it was nil!")
        
        
        if let apibaseUrl = apibaseUrl {
            XCTAssertEqual("https://www.omdbapi.com/?", apibaseUrl, "invalid base url!")
        }
        
    }
}
