//
//  ZupSearchMapperTest.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import XCTest

@testable import ZupTeste


class ZupSearchMapperTest: XCTestCase {
    
    
    func testCCOMDbSearchResponseMapper() {
        let searchResponseMapper =  CCOMDbSearchResponseMapper()
        let jsonString = "{\"Search\": [{\"Title\": \"O anthropos pou gyrise apo ti zesti\",\"Year\": \"1972\",\"imdbID\": \"tt0181294\",\"Type\": \"movie\"},]}"
        
        let jsonData = (jsonString as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let json = JSON(data:jsonData!,options: .mutableContainers)
        
        
        
        let movieSummaries : [MovieSummary]?  = searchResponseMapper.map(json) as? [MovieSummary]
        
        XCTAssertEqual(movieSummaries!.count, 1, "incorrect number of movies")
    }
    
    func testCCOMDbDetailResponseMapper() {
        let detailResponseMapper = CCOMDbDetailResponseMapper()
        
        let jsonString = "{\"Title\":\"The Social Network\",\"Year\":\"2010\",\"Rated\":\"PG-13\",\"Released\":\"2010-10-01\",\"Runtime\":\"120 min\",\"Genre\":\"Biography, Drama\",\"Director\":\"David Fincher\",\"Writer\":\"Aaron Sorkin (screenplay), Ben Mezrich (book)\",\"Actors\":\"Jesse Eisenberg, Rooney Mara, Bryan Barter, Dustin Fitzsimons\",\"Plot\":\"Harvard student Mark Zuckerberg creates the social networking site that would become known as Facebook, but is later sued by two brothers who claimed he stole their idea, and the cofounder who was later squeezed out of the business.\",\"Language\":\"English, French\",\"Country\":\"USA\",\"Awards\":\"Won 3 Oscars. Another 160 wins & 122 nominations.\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTM2ODk0NDAwMF5BMl5BanBnXkFtZTcwNTM1MDc2Mw@@._V1_SX300.jpg\",\"Metascore\":\"95\",\"imdbRating\":\"7.8\",\"imdbVotes\":\"391,202\",\"imdbID\":\"tt1285016\",\"Type\":\"movie\",\"Response\":\"True\"}"
        
        let jsonData = (jsonString as NSString).data(using: String.Encoding.utf8.rawValue)
        let json = JSON(data:jsonData!,options: .mutableContainers)
        let movieDetail : MovieDetail?  = detailResponseMapper.map(json) as? MovieDetail
        
        let title = movieDetail!.title!
        XCTAssertEqual(title, "The Social Network", "incorrect title")
        
        
    }
}
