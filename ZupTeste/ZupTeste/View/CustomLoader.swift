//
//  CustomLoader.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit


class CustomLoader : UIView {
    var filmRollLayer : CALayer?
    let side: CGFloat = 100.0
    let filmRollImage = UIImage(named: "FilmRoll")!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        
        filmRollLayer = CALayer()
        filmRollLayer?.frame = CGRect(
            x: 0,
            y: 0,
            width: side,
            height: side)
        

        filmRollLayer?.contents = filmRollImage.cgImage

        backgroundColor = Constants.Colors.loaderBackgroundColor
        layer.addSublayer(filmRollLayer!)
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        
        filmRollLayer = CALayer()
        filmRollLayer?.frame = CGRect(
            x: 0,
            y: 0,
            width: side,
            height: side)
        

        filmRollLayer?.contents = filmRollImage.cgImage
        backgroundColor = Constants.Colors.loaderBackgroundColor
        layer.addSublayer(filmRollLayer!)
    }
    
    
    
    override func layoutSubviews() {
        filmRollLayer?.position = center
    }
    
    func show() {
        isHidden = false
        filmRollLayer?.position = center
        //if already has animation, return
        if (filmRollLayer?.animation(forKey: "rotateFilmRoll")) != nil {
            return
        }
        
        //now add animation
        //filmRole?.position = self.center
        if let superview = superview {
            center = superview.center
        }
        
        let basicAnimation = CABasicAnimation()
        basicAnimation.keyPath = "transform.rotation"
        basicAnimation.toValue = 2 * M_PI
        basicAnimation.duration = 2
        basicAnimation.repeatCount = Float.infinity
        basicAnimation.isRemovedOnCompletion = false
        basicAnimation.fillMode = kCAFillModeForwards
        filmRollLayer?.add(basicAnimation, forKey: "rotateFilmRoll")
    }
    
    
    func hide() {
        if (filmRollLayer?.animation(forKey: "rotateFilmRoll")) != nil {
            filmRollLayer?.removeAnimation(forKey: "rotateFilmRoll")
        }
        isHidden = true
    }
    
    

    
}
