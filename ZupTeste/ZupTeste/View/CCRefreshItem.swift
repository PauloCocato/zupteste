//
//  CCRefreshItem.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit

class CCRefreshItem  {
    fileprivate var centerStart: CGPoint!
    fileprivate var centerEnd: CGPoint!
    unowned var view : UIView
    
    internal init(view:UIView,centerEnd:CGPoint,paralledRatio: CGFloat, sceneHeight: CGFloat) {
        self.view = view
        self.centerEnd = centerEnd
        centerStart = CGPoint(x: centerEnd.x,
            y: centerEnd.y + (paralledRatio * sceneHeight))
        
        self.view.center = centerStart
    }

    
    internal func updateViewPositionForPercentage(_ percentage:CGFloat) {
        view.center = CGPoint(
            x: centerStart.x + (centerEnd.x - centerStart.x) * percentage,
            y: centerEnd.y + (centerEnd.y - centerStart.y) * percentage)
    }
}
