//
//  CCRefreshView.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit
class CCRefreshView : UIView {
    fileprivate unowned var scrollView: UIScrollView
    fileprivate let sceneHeight :CGFloat = 120
    var alphaProgressPercentage :CGFloat = 0
    var progressPercentage:CGFloat = 0
    var refreshItems = [CCRefreshItem]()
    
    
    required init(coder aDecoder: NSCoder) {
        scrollView = UIScrollView()
        super.init(coder: aDecoder)!
        fatalError("init(coder:) has not been implemented")
    }
    
    
    init(frame:CGRect, scrollView:UIScrollView) {
        self.scrollView = scrollView
        super.init(frame: frame)
        backgroundColor = Constants.Colors.refreshViewBAckground
        setupRefreshItems()
    }
    
    
    func updateBackgroundColor() {
        self.alpha = alphaProgressPercentage
    }
    
    func updateRefreshItemPositions() {
        for refreshItem in refreshItems {
            refreshItem.updateViewPositionForPercentage(progressPercentage)
        }
    }
    
    
    fileprivate func setupRefreshItems() {
        let lionImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        lionImageView.image =  UIImage(named: "Lion")
        
        let frameImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        frameImageView.image = UIImage(named: "Frame")
        
        refreshItems  = [
            CCRefreshItem(view: lionImageView,
                centerEnd: CGPoint(x: bounds.midX, y: bounds.midY) ,
                paralledRatio: -0.8,
                sceneHeight: sceneHeight),
            
            CCRefreshItem(view: frameImageView,
                centerEnd: CGPoint(x: bounds.midX, y: bounds.midY - frameImageView.bounds.height  ),
                paralledRatio: 1,
                sceneHeight: sceneHeight)
            
        ]
        
        
        
        for refteshItem in refreshItems {
            addSubview(refteshItem.view)
        }
    }
}


extension CCRefreshView : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let refreshViewVisibleHeight :CGFloat = max(0, -(scrollView.contentOffset.y + scrollView.contentInset.top))
        
        alphaProgressPercentage = 1 - refreshViewVisibleHeight/sceneHeight
        progressPercentage = min(1.0 , refreshViewVisibleHeight / sceneHeight)
        
        
        
        updateBackgroundColor()
//        updateRefreshItemPositions()
    }
}

