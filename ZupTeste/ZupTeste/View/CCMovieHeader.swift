//
//  MovieHeader.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit
class CCMovieHeader : UICollectionReusableView {
    
    @IBOutlet weak var lblMovieTitle: UILabel!
    
    var movieTitle : String? {
        didSet {
            if let movieTitle = movieTitle {
                lblMovieTitle.text = movieTitle
            }
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 1)
        topBorder.backgroundColor = Constants.Colors.cellHeaderBottomBorder.cgColor
        
        layer.addSublayer(topBorder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        lblMovieTitle.textColor = Constants.Colors.searchBarColor
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        lblMovieTitle.text = ""
    }
}
