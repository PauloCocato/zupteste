//
//  CCMovieCell.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit

class CCMovieCell : UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var lblActors: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var viewLoader: CustomLoader!
    
    
    fileprivate var title: String?
    fileprivate var genre: String?
    fileprivate var actors: String?
    fileprivate var poster: String?
    fileprivate var imdbRating: String?
    fileprivate var imdbId: String?
    
    
    var movieDetail : MovieDetail? {
        didSet {
            if let movieDetail = movieDetail {

                if let actors = movieDetail.actors {
                    lblActors.text = lblActors.text! + " " + actors
                    lblActors.sizeToFit()
                }
                if let genre = movieDetail.genre {
                    lblGenre.text = lblGenre.text! + " " + genre
                    lblGenre.sizeToFit()
                }
            } else {
                lblActors.text = "Stars:"
                lblGenre.text = "Genre:"
            }
            
            
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundView?.backgroundColor = Constants.Colors.cellBackground
        
    }
    
    var image: UIImage? {
        didSet {
            if let image = image {
                movieImage.image = image
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        movieDetail = nil
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //resetting cell
        movieDetail = nil
        viewLoader.hide()
        
        
        //remove image loader
        movieImage.image = (UIApplication.shared.delegate as! AppDelegate).noPhotoImage
        
        
    }
}










