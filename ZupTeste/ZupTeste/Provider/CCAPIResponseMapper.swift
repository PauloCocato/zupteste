//
//  CCAPIResponseMapper.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation



protocol CCAPIResponseMapper {
     func map(_: JSON?) -> AnyObject?
}
