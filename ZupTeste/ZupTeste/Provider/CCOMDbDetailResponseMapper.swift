//
//  CCOMDbDetailResponseMapper.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

class CCOMDbDetailResponseMapper : CCAPIResponseMapper {
    func map(_ responseJson: JSON?) -> AnyObject? {
        var movieDetail : MovieDetail
        if let responseJson = responseJson {
            movieDetail = MovieDetail(
                title:      responseJson["Title"].string,
                year:       responseJson["Year"].string,
                rated:      responseJson["Rated"].string,
                released:   responseJson["Released"].string,
                runtime:    responseJson["Runtime"].string,
                genre:      responseJson["Genre"].string,
                director:   responseJson["Director"].string,
                writer:     responseJson["Writer"].string,
                actors:     responseJson["Actors"].string,
                plot:       responseJson["Plot"].string,
                language:   responseJson["Language"].string,
                country:    responseJson["Country"].string,
                awards:     responseJson["Awards"].string,
                poster:     responseJson["Poster"].string,
                metascore:  responseJson["Metascore"].string,
                imdbRating: responseJson["imdbRating"].string,
                imdbVotes:  responseJson["imdbVotes"].string,
                imdbId:     responseJson["imdbID"].string,
                type:       MovieType.getMovieType(responseJson["Type"].string)
            )
            return movieDetail
        }
        return nil
    }
}
