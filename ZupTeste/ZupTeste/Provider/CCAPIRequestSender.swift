//
//  CCAPIRequestSender.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

typealias apiRequestCompletionHandler = (_ data:JSON? , _ requestError:NSError?) -> Void
typealias apiImageRequestCompletionHandler = (_ image:UIImage?, _ requestError:NSError?) -> Void




class CCAPIRequestSender {
    
    //MARK: - Private
    fileprivate var request:Alamofire.Request? {
        didSet {
            //cancelling pending requests, if it exist
            oldValue?.cancel()
        }
    }
    
    fileprivate var downloadImageRequest : Alamofire.Request? {
        didSet {
            //cancelling pending requests, if it exist
            oldValue?.cancel()
        }
    }
    
    
    //MARK: - Public
    init() {
        
    }
    
    //from Alamofire documentation: "Networking in Alamofire is done asynchronously."
    //this means that we don't need to explicitly creating a new async thread for network calls, and 
    //alamofire automatically does it for us. 
    //Asynchronously send get request to the network
    func getRequestForResource(_ resource:String,
        parameters:[String:AnyObject]?,
        completionHandler:@escaping apiRequestCompletionHandler) -> Void {
        
        request = Alamofire.request(resource, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                //if there is an error, execute callback with error
                if response.error != nil {
                    completionHandler(nil, response.error as NSError?)
                }
                    //otherwise execute callback with json data
                else {
                    if let data: AnyObject = response.data as AnyObject? {
                        let jsonData = JSON(data)
                        completionHandler(jsonData, nil)
                    } else {
                        completionHandler(nil, nil)
                    }
                    
                }
        }
    }
    
    
    
    func getImage(_ resource:String,completionHandler: @escaping apiImageRequestCompletionHandler) -> Void {
        downloadImageRequest = Alamofire.request(resource).response { response in
                if response.error != nil {
                    completionHandler(nil, response.error as NSError?)
                }
            
                var image : UIImage?
                //going on a background thread to convert the data which is of type AnyObject to UIImage
                let concurrentOperation = OperationQueue()
                concurrentOperation.addOperation({ () -> Void in
                    image = UIImage(data:response.data!)
                    
                    
                    //going to the main thread to return the UIImage so the UI can access it safely
                    OperationQueue.main.addOperation({ () -> Void in
                        completionHandler(image, nil)
                    })
                })
            
            
            }
    }
    
    
}





















