//
//  SaveMovieDatabaseProvider.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation


internal class SaveMovieDatabaseProvider {
    
    var movieSummaries = [MovieDetail]()
    
    // Save items in cache
    ///
    /// - Parameters:
    ///    - movie: movie saved by user
    /// - Returns: Bool of operation success
    func saveItemsInCache(movie: MovieDetail) -> Bool {
        do {
            let realmObject = movie.asRealmObject()
            
            try ZUPDatabaseProvider.sharedProvider.save(object: realmObject, update: true)
            
            return true
        } catch {
            return false
        }
    }
    
    /// Gets an array of items from cache with a foreign key
    ///
    /// - Returns: Array of cache items
    func fetchFromCache() -> [MovieDetail] {
        let resultRealm = ZUPDatabaseProvider.sharedProvider.fetchAll(MovieDetailRealm.self)

        return resultRealm.map { return MovieDetail(withRealmObject: $0) }
    }
    

}
