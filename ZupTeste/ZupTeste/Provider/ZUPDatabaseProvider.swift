//
//  ZUODatabaseProvider.swift
//  ZupApp
//
//  Created by Paulo Victor Cocato on 01/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import RealmSwift

/// Provider to manage database and cache
/// Configs
internal class ZUPDatabaseProvider {
    
    static let sharedProvider = ZUPDatabaseProvider()
    private static var userKey: String?
    
    
    /// Setup method to configure database properties by user
    ///
    /// - parameter user: user unique id
    func setupForUser(_ user: String) {
        
        ZUPDatabaseProvider.userKey = user
        configureRealm(withKey: key())
    }
    
    
    /// Default realm
    ///
    /// - returns: the default realm set for the logged user
    fileprivate func defaultRealm() -> Realm {
        do {
            configureRealm(withKey: key())
            let realm = try Realm()
            return realm
        } catch {
            fatalError("Trying to open database with wrong configuraion. Aborting.")
        }
    }
    
    
    /// Configure the path for database file based on user unique id
    ///
    /// - parameter withKey: security access key
    /// - parameter user:    user unique id
    private func configureRealm(withKey: NSData) {
        var configuration = Realm.Configuration(encryptionKey: key() as Data)
        
        if configuration.fileURL != nil {
            configuration.fileURL = configuration.fileURL!.deletingLastPathComponent().appendingPathComponent("database.realm")
        }
        
        Realm.Configuration.defaultConfiguration = configuration
    }
    
    
    /// Generate, stores and recover the security key
    ///
    /// - returns: key
    private func key() -> NSData {
        
        // Identifier for our keychain entry - should be unique
        let keychainIdentifier = "com.zup.databasekey"
        let keychainIdentifierData = keychainIdentifier.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        // First check in the keychain for an existing key
        var query: [NSString: AnyObject] = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecReturnData: true as AnyObject
        ]
        
        var dataTypeRef: AnyObject?
        var status = withUnsafeMutablePointer(to: &dataTypeRef) { SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0)) }
        if status == errSecSuccess {
            guard let data = dataTypeRef as? NSData else {
                fatalError("Failed to retrieve keychain")
            }
            return data
        }
        
        // No pre-existing key from this application, so generate a new one
        let keyData = NSMutableData(length: 64)!
        let result = SecRandomCopyBytes(kSecRandomDefault, 64, keyData.mutableBytes.bindMemory(to: UInt8.self, capacity: 64))
        assert(result == 0, "Failed to get random bytes")
        
        // Store the key in the keychain
        query = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecValueData: keyData
        ]
        
        status = SecItemAdd(query as CFDictionary, nil)
        assert(status == errSecSuccess, "Failed to insert the new key in the keychain")
        
        return keyData
    }
}

/// Realm Methods
extension ZUPDatabaseProvider {
    
    
    /// Save Realm Object or update an existing one. Objects must always user a primary key.
    ///
    /// - parameter object: object to insert
    /// - parameter update: if true, if an object already exists on database, it is updated with the new values. Otherwise it is inserted.
    ///
    /// - throws: An `NSError` if the transaction could not be completed successfully
    func save(object: Object, update: Bool = true) throws {
        
        let realm = self.defaultRealm()
        try realm.write {
            realm.add(object, update: update)
        }
    }
    
    /// Save Realm Array Object or update existing ones. Objects must always user a primary key.
    ///
    /// - parameter object: object to insert
    /// - parameter update: if true, if an object already exists on database, it is updated with the new values. Otherwise it is inserted.
    ///
    /// - throws: An `NSError` if the transaction could not be completed successfully
    func save(objects: [Object], update: Bool = true) throws {
        
        let realm = self.defaultRealm()
        try realm.write {
            realm.add(objects, update: update)
            try realm.commitWrite()
        }
    }
    
    /// Fetch all objects of T type
    ///
    /// - parameter type: class type object to return
    ///
    /// - returns: All objects for the given class name as dynamic objects
    func fetchAll<T: RealmSwift.Object>(_ type: T.Type) -> RealmSwift.Results<T> {
        
        let realm = self.defaultRealm()
        return realm.objects(type)
    }
    
    
    /// Fetch objects of T type
    ///
    /// - parameter type:      class type object to return
    /// - parameter predicate: predicate to filter results
    ///
    /// - returns: filtered objects for the given class name as dynamic objects
    func query<T: RealmSwift.Object>(_ type: T.Type, predicate: NSPredicate) -> RealmSwift.Results<T> {
        
        let realm = self.defaultRealm()
        let result = realm.objects(type).filter(predicate)
        
        return result
    }
    
    
    /// Deletes all the items in the array
    ///
    /// - Parameter objects: The items to delete
    func delete(objects: [Object]) throws {
        let realm = self.defaultRealm()
        
        try realm.write {
            realm.delete(objects)
            try realm.commitWrite()
        }
    }
    
    /// Deletes all the items in the array
    ///
    /// - Parameter objects: The items to delete
    func delete(object:Object) throws {
        let realm = self.defaultRealm()
        
        try realm.write {
            realm.delete(object)
            try realm.commitWrite()
        }
    }
}
