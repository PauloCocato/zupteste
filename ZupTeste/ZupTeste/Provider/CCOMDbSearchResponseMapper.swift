//
//  CCOMDbResponseMapper.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation


class CCOMDbSearchResponseMapper : CCAPIResponseMapper {
    func map(_ responseJson: JSON?) -> AnyObject? {
        var movieSummaries = [MovieSummary]()
        
        if let responseJson = responseJson  {
                let searchResult = (responseJson["Search"]).arrayValue
            
                for movieItem in searchResult {
                    let movieSummary = MovieSummary(title: movieItem["Title"].stringValue,
                        year: movieItem["Year"].stringValue,
                        imdbId: movieItem["imdbID"].stringValue,
                        type: movieItem["Type"].stringValue)
                
                    movieSummaries.append(movieSummary)
                }
            return movieSummaries as AnyObject?
        }
        return nil
    }
}
