//
//  CCOMDbConnector.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import UIKit

typealias searchMovieByNameCompletionHandler = (_ success:Bool,_ result:[MovieSummary]? , _ error:String?) -> Void
typealias getMoviesDetailsByIdCompletionHandler = (_ success:Bool, _ result: MovieDetail?, _ error: String?) -> Void


typealias getImageCompletionHandler  = (_ succes:Bool, _ image: UIImage? , _ error: String?) -> Void

class CCOMDbConnector {
    
    fileprivate let apiRequestSender = CCAPIRequestSender()
    fileprivate let apibaseUrl = ConfigUtil.getConfigValueForKey("apiBaseUrl")
    
    
    //searches movies by name
    func searchMovieByName(_ name:String,
        type:String?,
        year:String?,
        completionHandler:@escaping searchMovieByNameCompletionHandler) -> Void {
            var parameters = "s=" + name
            
            if let type = type {
               parameters = "&t=" + type
            }
            
            if let year = year {
                parameters = "&y=" + year
            }
        
        let searchUrl = apibaseUrl! + parameters
            
        apiRequestSender.getRequestForResource(searchUrl,
            parameters: nil as [String : AnyObject]?) { (data, requestError) -> Void in
                
                if requestError != nil {
                    let errorString = requestError!.description
                    completionHandler(false, nil, errorString)
                } else {
                    let mapper = CCOMDbSearchResponseMapper()
                    let movieSummaries = mapper.map(data) as? [MovieSummary]
                    completionHandler(true, movieSummaries, nil)
                    
                }
                
            }
    }
    
    
    func getMovieById(_ imdbId:String,
        completionHandler:@escaping getMoviesDetailsByIdCompletionHandler) -> Void {
        
            let searchUrl = apibaseUrl! + "i=" + imdbId
        
            apiRequestSender.getRequestForResource(searchUrl,
                parameters:nil) { (data, requestError) -> Void in
                    if requestError != nil {
                        let errorString = requestError!.description
                        completionHandler(false, nil, errorString)
                    } else {
                        let mapper = CCOMDbDetailResponseMapper()
                        let movieDetail = mapper.map(data) as? MovieDetail
                        
                        completionHandler(true, movieDetail, nil)
                    }
                    
            }
    }
    
    
    
    func getImageFromUrl(_ imageResource: String,
        completionHandler:@escaping getImageCompletionHandler) -> Void {
            apiRequestSender.getImage(imageResource,
                completionHandler: { (image, requestError) -> Void in
                    if requestError != nil {
                        let errorString = requestError!.description
                        completionHandler(false, nil, errorString)
                    }
                    else {
                        if image == nil {
                            let errorString = "Couldn't download the image"
                            completionHandler(false, nil, errorString)
                        } else {
                            completionHandler(true, image, nil)
                        }
                    }
            })
    }
    
    
}









