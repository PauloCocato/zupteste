//
//  ConfigUtil.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

class ConfigUtil {
    
    private static var __once: () = {
                Static.instance = NSDictionary(contentsOfFile:
                    Bundle.main.path(forResource: "Configurations", ofType: "plist")!) as? Dictionary<String,String>
            }()
    
    fileprivate struct Static {
        static var onceToken : Int = 0
        static var instance : [String:String]? = nil
    }
    
    
    //computed property to get configurations, it's defined as a singleton not to access the file system everytime we want to read configs, this causes some performance improvements
    fileprivate class var configs:[String:String]? {
        get {
            _ = ConfigUtil.__once
            return Static.instance
        }
    }
    
    
    
    class func getConfigValueForKey(_ key:String) -> String? {
        if let configs = configs {
            return configs[key]
        }
        return nil
    }
    
    
    
    
}










