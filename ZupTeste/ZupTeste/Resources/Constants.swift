//
//  Constants.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct ViewTags {
        //some random number which helps us to access loader view when we want to remove it from the cell
        static let loaderViewTag = 412
    }
    
    //&plot=full&r=json

    
    
    struct APIQueryStringParameters {
        static let searchTerm = "s"
        static let searchType = "type"
        static let year = "y"
        static let plot = "plot"
        static let resource = "r"
        static let imdbId = "i"
    }
    
    struct Colors {
        static let navBarColor = UIColor(red: 80.0/255.0, green: 96.0/255.0, blue: 136.0/255.0, alpha: 1.0)
        
        static let searchBarColor = UIColor(red: 80.0/255.0, green: 96.0/255.0, blue: 136.0/255.0, alpha: 1.0)
        
        static let cellBackground = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1)
        
        static let cellHeaderBottomBorder = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        
        static let refreshViewBAckground = UIColor(red: 57.0/255.0, green: 91.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        
        static let loaderBackgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.2)
        
    }
}










