//
//  MovieType.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

enum MovieType : String {
    case Movie = "Movie"
    case Series = "Series"
    case Episode = "Episode"
    case Unknown = "Unknown"
    
    static func getMovieType(_ movieTypeString:String?) -> MovieType {
        if let movieTypeString = movieTypeString {
            switch movieTypeString.lowercased() {
            case "movie" :
                return .Movie
            case "series" :
                return .Series
            case "episode" :
                return .Episode
            default :
                return .Unknown
            }
        }
        return .Unknown
    }
    
    static func getMovieTypeString(_ movieType:MovieType?) -> String {
        if let movieType = movieType {
            switch movieType {
            case .Movie :
                return "movie"
            case .Series :
                return "series"
            case .Episode :
                return "episode"
            default :
                return ""
            }
        }
        return ""
    }
}
