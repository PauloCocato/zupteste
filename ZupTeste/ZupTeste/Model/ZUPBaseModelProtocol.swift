//
//  ZUPBaseModelProtocol.swift
//  ZupApp
//
//  Created by Paulo Victor Cocato on 13/03/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

protocol ZUPBaseModelProtocol {
    static func fillWithDictionary<T>(_ variable: inout T, key: String, dictionary: Dictionary<String, AnyObject>)
}

extension ZUPBaseModelProtocol {
    
    /**
     fill a property from a dictionary
     
     - parameter variable:      field to be filled
     - parameter key:        key from dictionary
     - parameter dictionary: dictionary
     */
    static func fillWithDictionary<T>(_ variable: inout T, key: String, dictionary: Dictionary<String, AnyObject>) {
        if let tempField = dictionary[key] as? T {
            variable = tempField
        }
    }
}
