//
//  MovieDetail.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

class MovieDetail {
    let title: String?
    let year: String?
    let rated: String?
    let released: String?
    let runtime: String?
    let genre: String?
    let director: String?
    let writer: String?
    let actors: String?
    let plot: String?
    let language: String?
    let country: String?
    let awards: String?
    let poster: String?
    let metascore: String?
    let imdbRating: String?
    let imdbVotes: String?
    let imdbId: String?
    let type: MovieType?

    init(imdbId: String? , title: String?, type: MovieType?,year : String?) {
        self.imdbId = imdbId
        self.title = title
        self.type = type
        self.year = year
        
        self.rated = ""
        self.released = ""
        self.runtime = ""
        self.genre = ""
        self.director = ""
        self.writer = ""
        self.actors = ""
        self.plot = ""
        self.language = ""
        self.country = ""
        self.awards = ""
        self.poster = ""
        self.metascore = ""
        self.imdbRating = ""
        self.imdbVotes = ""
    }

    init(title: String?,
         year: String?,
         rated: String?,
         released: String?,
         runtime: String?,
         genre: String?,
         director: String?,
         writer: String?,
         actors: String?,
         plot: String?,
         language: String?,
         country: String?,
         awards: String?,
         poster: String?,
         metascore: String?,
         imdbRating: String?,
         imdbVotes: String?,
         imdbId: String?,
         type: MovieType?) {

        self.title = title
        self.year = year
        self.rated = rated
        self.released = released
        self.runtime = runtime
        self.genre = genre
        self.director = director
        self.writer = writer
        self.actors = actors
        self.plot = plot
        self.language = language
        self.country = country
        self.awards = awards
        self.poster = poster
        self.metascore = metascore
        self.imdbRating = imdbRating
        self.imdbVotes = imdbVotes
        self.imdbId = imdbId
        self.type = type
    }
    
    
    /// initialize a AMRRealmSizeModel from realm object
    ///
    /// - Parameter realmObject: the size realm object
    init(withRealmObject realmObject: MovieDetailRealm) {
        
        title = realmObject.title
        year = realmObject.year
        rated = realmObject.rated
        released = realmObject.released
        runtime = realmObject.runtime
        genre = realmObject.genre
        director = realmObject.director
        writer = realmObject.writer
        actors = realmObject.actors
        plot = realmObject.plot
        language = realmObject.language
        country = realmObject.country
        awards = realmObject.awards
        poster = realmObject.poster
        metascore = realmObject.metascore
        imdbRating = realmObject.imdbRating
        imdbVotes = realmObject.imdbVotes
        imdbId = realmObject.imdbId
        type = MovieType.getMovieType(realmObject.type)
    }
    
    /// Creates a AMRRealmSizeModel instance
    ///
    /// - Returns: an instance of AMRRealmSizeModel
    func asRealmObject() -> MovieDetailRealm {
        let realmObject = MovieDetailRealm();
        
        realmObject.title = title
        realmObject.year = year
        realmObject.rated = rated
        realmObject.released = released
        realmObject.runtime = runtime
        realmObject.genre = genre
        realmObject.director = director
        realmObject.writer = writer
        realmObject.actors = actors
        realmObject.plot = plot
        realmObject.language = language
        realmObject.country = country
        realmObject.awards = awards
        realmObject.poster = poster
        realmObject.metascore = metascore
        realmObject.imdbRating = imdbRating
        realmObject.imdbVotes = imdbVotes
        realmObject.imdbId = imdbId!
        realmObject.type = MovieType.getMovieTypeString(type)
        
        return realmObject
    }

}


extension MovieDetail : CustomStringConvertible {
    var description: String {
        get {
            var desc : String = ""

            if let title = title {
                desc = "title: \(title)"
            }
            if let year = year {
                desc = desc + ", year: \(year)"
            }
            if let imdbId = imdbId {
                desc = desc + ", imdbId: \(imdbId)"
            }
            return desc
        }
    }

}
