//
//  CCMovieViewModel.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import UIKit

struct CCMovieViewModel  {
    var imdbId : String = ""
    var movieDetails : MovieDetail?
    var isRequestCompleted : Bool = false
    var isRequesInProgress : Bool = false
    var indexPath : IndexPath?
    
    var image : UIImage?
    var isImageRequestCompleted: Bool  = false
    var isImageRequestInProgress :  Bool = false
    
    init(movieSummary:MovieSummary) {
        movieDetails = MovieDetail(imdbId: movieSummary.imdbId,
            title: movieSummary.title,
            type: movieSummary.type,
            year: movieSummary.year)
        imdbId = movieSummary.imdbId!
    }
    
    init(movieDetail:MovieDetail) {
        movieDetails = MovieDetail(imdbId: movieDetail.imdbId,
                                   title: movieDetail.title,
                                   type: movieDetail.type,
                                   year: movieDetail.year)
        
        imdbId = movieDetail.imdbId!
    }
}
