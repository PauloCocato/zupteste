//
//  Summary.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation

class MovieSummary  {
    let title:  String?
    let year:   String?
    let imdbId: String?
    let type:   MovieType?
    
    init(title:String?,year:String?,imdbId:String?, type:String?) {
        self.title = title
        self.year = year
        self.imdbId = imdbId
        self.type = MovieType.getMovieType(type)
    }
}


extension MovieSummary : CustomStringConvertible {
    var description: String {
        get {
            var desc : String = ""
            
            if let title = title {
                desc = "title: \(title)"
            }
            if let year = year {
                desc = desc + ", year: \(year)"
            }
            if let imdbId = imdbId {
                desc = desc + ", imdbId: \(imdbId)"
            }
            if let type = type {
                desc = desc + ", type: \(type)"
            }
            return desc
        }
    }
}





