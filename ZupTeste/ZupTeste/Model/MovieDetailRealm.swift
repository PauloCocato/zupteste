//
//  MovieDetailRealm.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import Foundation
import Realm
import RealmSwift


class MovieDetailRealm: Object {
    // MARK: public properties
    
    dynamic var imdbId: String = ""
    dynamic var title: String?
    dynamic var year: String?
    dynamic var rated: String?
    dynamic var released: String?
    dynamic var runtime: String?
    dynamic var genre: String?
    dynamic var director: String?
    dynamic var writer: String?
    dynamic var actors: String?
    dynamic var plot: String?
    dynamic var language: String?
    dynamic var country: String?
    dynamic var awards: String?
    dynamic var poster: String?
    dynamic var metascore: String?
    dynamic var imdbRating: String?
    dynamic var imdbVotes: String?
    dynamic var type: String?

    
    //Realm primary key
    override static func primaryKey() -> String {
        return "imdbId"
    }
}
