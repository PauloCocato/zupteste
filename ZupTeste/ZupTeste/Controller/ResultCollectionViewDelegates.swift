//
//  ResultCollectionViewDataSource.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit


//MARK: - Search delegate implementation
extension ResultCollectionView : SearchDelegate, LoadDelegate {
    func didSearchForTerm(_ searchTerm: String?) {
        if let searchTerm = searchTerm {
            if searchTerm.characters.count  > 0 {
                //user has searched for a new term, so first empty the current data source
                movies?.removeAll(keepingCapacity: false)
                collectionView?.reloadData()
            }
        }
        
        //remove the blocker view and child search view controller
        UIView.animate(withDuration: addRemoveSearchChildViewControllerAnimationDuration,
            delay: 0,
            options: .curveEaseOut,
            animations: { () -> Void in
                let keyWindow = UIApplication.shared.keyWindow!
                let searchViewHeight = keyWindow.frame.height / 8.0
                self.searchVC.view.frame.origin.y = -searchViewHeight
                self.blockerView.backgroundColor = UIColor.clear
                
            }) { (finished) -> Void in
                self.searchVC.willMove(toParentViewController: nil)
                self.searchVC.view.removeFromSuperview()
                self.searchVC.removeFromParentViewController()
                self.blockerView.removeFromSuperview()
        }
        
        
        
        if searchTerm == nil {
            return
        }
        
        if let searchTerm = searchTerm {
            if searchTerm.characters.count == 0 {
                return
            }
            
            
            //add activity indicator
            activityIndicator.startAnimating()
            
            //search for the term use has entered if there is any
            if searchTerm.characters.count > 0 {
                connector.searchMovieByName(searchTerm, type: nil, year: nil) { (success, result, error) -> Void in
                    self.activityIndicator.stopAnimating()
                    if(success) {
                        self.movies = result?.map({(movieSummary) -> CCMovieViewModel in
                            return CCMovieViewModel(movieSummary: movieSummary)})
                        self.collectionView?.reloadData()
                    } else {
                        //todo:show error message
                    }
                }
            }
        }
    }
    
    func didLoad() {
        let database = SaveMovieDatabaseProvider()
        let result = database.fetchFromCache()
        
        self.movies = result.map({(movieDetail) -> CCMovieViewModel in
            return CCMovieViewModel(movieDetail: movieDetail)})
        
        self.collectionView?.reloadData()
    }
    
    
    //MARK: - collection view data source delegate implementation
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let movies = movies {
            return movies.count
        }
        return 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if movies != nil {
            return 1//movies.count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "movieHeader", for: indexPath) as! CCMovieHeader
        
        let movieTitle = movies![indexPath.section]
        header.movieTitle = movieTitle.movieDetails?.title
        
        return header
    }
    
    
    //getting the cell for movie, this is where we lazy load everything which is going to be shown for performance improvement
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CCMovieCell
        
        
        var cellDataSource : CCMovieViewModel = movies![indexPath.section]
        cellDataSource.indexPath = indexPath
        
        if (cellDataSource.isRequestCompleted == true) {
            cell.movieDetail = cellDataSource.movieDetails
            
            
            //lazyly download the movie poster
            if let imageUrl = cellDataSource.movieDetails?.poster {
                //now start getting the image for the cell
                if (cellDataSource.isImageRequestCompleted == true) {
                    cell.movieImage.image = cellDataSource.image
                    cell.viewLoader.hide()
                } else if (cellDataSource.isImageRequestInProgress) {
                    
                    
                } else {
                    //now lazy loading image for current visible cell
                    self.movies![indexPath.section].isImageRequestInProgress = true
                    
                    self.connector.getImageFromUrl(imageUrl, completionHandler: { (imageDownloadSuccess, image, error) -> Void in
                        self.movies![indexPath.section].isImageRequestInProgress = false
                        self.movies![indexPath.section].isImageRequestCompleted = true
                        
                        if imageDownloadSuccess {
                            self.movies![indexPath.section].image = image
                            if (self.isItemAssociatedWithCurrentDatasourceItemVisible(collectionView, dataSourceIndexPath: cellDataSource.indexPath!)) {
                                collectionView.reloadData()
                            }
                        } else {
                            //let's download it again
                            self.movies![indexPath.section].isImageRequestCompleted = false
                        }
                    })
                    
                    
                }
            }
            
            
        } else if (cellDataSource.isRequesInProgress) {
            cell.viewLoader.show()
        }
        else {
            cell.viewLoader.show()
            //now lazy loading current visible cell
            self.movies![indexPath.section].isRequesInProgress = true

            connector.getMovieById(cellDataSource.imdbId, completionHandler: { (success, movieDetail, error) -> Void in
                self.movies![indexPath.section].isRequesInProgress = false
                self.movies![indexPath.section].isRequestCompleted = true
                
                
                if success {
                    self.movies![indexPath.section].movieDetails = movieDetail
                    
                    //if cell is currently visible
                    if (self.isItemAssociatedWithCurrentDatasourceItemVisible(collectionView, dataSourceIndexPath: cellDataSource.indexPath!)) {
                        collectionView.reloadData()
                    }
                } else {
                    //lets download it again
                    self.movies![indexPath.section].isRequestCompleted = false
                }
                

            })
            
        }
        return cell
    }
    
    
    func isItemAssociatedWithCurrentDatasourceItemVisible(_ collectionView: UICollectionView,dataSourceIndexPath: IndexPath) -> Bool {
        return (collectionView.indexPathsForVisibleItems as [IndexPath]).filter({indexPath in indexPath == dataSourceIndexPath}).count > 0
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        dummyRefreshView.scrollViewDidScroll(scrollView)
    }
}


