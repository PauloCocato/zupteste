//
//  SearchViewController.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    weak var delegate : SearchDelegate?
    weak var didLoadDelegate : LoadDelegate?
    
    @IBOutlet weak var txtSearchTerm: UITextField!
    
    @IBAction func btnCancelTapped(_ sender: AnyObject) {
        searchForTerm(nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearchTerm.leftViewMode = .always
        
        _ = txtSearchTerm.bounds.height - 10

        view.backgroundColor = Constants.Colors.searchBarColor
        
        if let didLoadDelegate = didLoadDelegate {
            didLoadDelegate.didLoad()
        }

    }
    
    
    func searchForTerm(_ term:String?) {
        if let delegate = delegate {
            delegate.didSearchForTerm(term)
        }
    }
}



//MARK: - UITextField delegate implementation
extension SearchViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtSearchTerm  {
            searchForTerm(textField.text)
            textField.text = ""
        }
        return true
    }
}














