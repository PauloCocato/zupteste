//
//  ResultCollectionView.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit



class ResultCollectionView: UICollectionViewController {
    //movie cell reuse identifier
    let cellReuseIdentifier = "movieCell"
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var dummyRefreshView : CCRefreshView!

    
    fileprivate let refreshViewHeight : CGFloat = 200
    
    
    let blockerView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    let addRemoveSearchChildViewControllerAnimationDuration = 0.2
    let searchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchBox") as! SearchViewController
    

    //data source of the page
    var movies : [CCMovieViewModel]? = [CCMovieViewModel]()
    var connector = CCOMDbConnector()
    //used for tracking selected item when we want to email the movie
    var selectedItemIndexPath : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = Constants.Colors.navBarColor
        
        collectionView!.backgroundColor = Constants.Colors.cellBackground
        
        let cellHeight = min(410, (4 * view.frame.height) / 5)
        
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: (view.frame).width, height: cellHeight)
        
        dummyRefreshView = CCRefreshView(
            frame:
            CGRect(x: 0, y: -refreshViewHeight, width: view.frame.width, height: refreshViewHeight),
            scrollView: collectionView!)
        //dummyRefreshView.translatesAutoresizingMaskIntoConstraints(false)
        collectionView!.insertSubview(dummyRefreshView, at: 0)
        
        didLoad()
    }
    
    @IBAction func btnSearchTapped(_ sender: AnyObject) {
        
        //adding search controller on top of all view to the key window
        searchVC.delegate = self
        
        let keyWindow = UIApplication.shared.keyWindow!
        
        let searchViewHeight = keyWindow.frame.height / 8.0
        
        keyWindow.addSubview(searchVC.view)
        addChildViewController(searchVC)
        searchVC.didMove(toParentViewController: self)
        
        searchVC.view.frame = CGRect(x: keyWindow.frame.origin.x,
            y: -searchViewHeight,
            width: (keyWindow.frame).width,
            height: searchViewHeight)
        
        blockerView.frame = self.view.frame
        blockerView.backgroundColor = UIColor.clear
        
        self.view.insertSubview(blockerView, belowSubview: searchVC.view)

        UIView.animate(withDuration: addRemoveSearchChildViewControllerAnimationDuration,
            delay: 0,
            options: .curveEaseOut,
            animations: { () -> Void in
                self.searchVC.view.frame.origin.y = 0
                self.blockerView.backgroundColor = UIColor(red: 173.0 / 255.0, green: 173.0 / 255.0, blue: 173.0 / 255.0, alpha: 0.4)
            }) { (finished) -> Void in
        }
    }
    
    @IBAction func btnRefresgTapped(_ sender: AnyObject) {
        didLoad()
    }
}






