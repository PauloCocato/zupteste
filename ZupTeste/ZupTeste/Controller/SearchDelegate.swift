//
//  SearchDelegate.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//


protocol SearchDelegate : class {
    func didSearchForTerm(_ searchTerm:String?) -> Void
}


protocol LoadDelegate : class {
    func didLoad() -> Void
}
