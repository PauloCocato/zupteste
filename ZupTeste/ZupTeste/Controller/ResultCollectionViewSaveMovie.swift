//
//  ResultCollectionViewEmailSending.swift
//  ZupTeste
//
//  Created by Paulo Victor Cocato on 13/02/17.
//  Copyright © 2017 Paulo Victor Cocato. All rights reserved.
//

import UIKit
import MessageUI

extension ResultCollectionView {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //selectedItemIndexPath
        let alertController = UIAlertController(title: "Salvar Filme", message: "Você gostaria de salvar esse filme?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let saveAction = UIAlertAction(title: "Salvar", style: .default) { (action) -> Void in
            let movieDetails = self.movies![indexPath.section].movieDetails
            self.saveMovie(movie: movieDetails!)
        }
        alertController.addAction(saveAction)
        
        present(alertController, animated: true, completion: nil)
   
    }
    
    func saveMovie(movie: MovieDetail) {
        let database = SaveMovieDatabaseProvider()
        let verify:Bool = database.saveItemsInCache(movie: movie)
        
        if verify {
          print("Ok")
        }
    }
}












